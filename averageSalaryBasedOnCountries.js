const data = require('./drill1');

let result = data.reduce(function(initial,current){
    if (!initial[current.location]){
        initial[current.location] = {countryCount: 0,countrySalary: 0, countryAverageSalary: 0}
    }

    initial[current.location].countryCount += 1
    initial[current.location].countrySalary += (parseFloat(current.salary.replace('$', ' '))*10000)/(initial[current.location].countryCount);
    initial[current.location].countryAverageSalary = (initial[current.location].countrySalary)/(initial[current.location].countryCount)
    
    return initial
},{})
let locations = Object.entries(result)
//console.log(locations)
const newResult = locations.reduce(function(initialvalue, currentvalue){
    initialvalue[currentvalue[0]] = currentvalue[1].countryAverageSalary
    return initialvalue
}, {})
console.log(newResult)